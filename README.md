# Calculator on x86 16-bit assembly

First you need to install DOSBOX or MS DOS. Then Borland Turbo Assembler.
Then run `compile.bat calc.asm` or `compile.bat sound.asm`.
 You should find a new .exe file: either calc.exe or sound.exe.
 You can run them both in 16-bit DOS (eg. MS DOS).

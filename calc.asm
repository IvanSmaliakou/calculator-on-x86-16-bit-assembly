.MODEL SMALL
.STACK 100h
.DATA
    message_error db "error. not a number", 10, 13, '$'
    message_error_operation db "error. not an operation", 13, 10, '$'
    message_enter_first_number db "enter the first number", 10, 13, '$'
    message_enter_second_number db "enter the second number", 10, 13, '$'
    message_enter_operation db "enter operation", 10, 13, '$'
    buff db 100 dup (?)
    actual_len db ?
    first_number dw ?
    second_number dw ?
.CODE

jmp start 

; address of the first byte of invitation is on the top of the stack.
; string is stored in the buff.
; actual len is stored in the var actual_len.
input_str PROC NEAR
    push bp
    mov bp, sp
    push ax
    push dx

    mov ah, 09h; invite to enter string
    mov dx, [bp+4]
    int 21h

    mov [buff], 100d
    lea dx, buff
    mov ah, 0Ah
    int 21h 
    mov al, [buff+1]
    mov actual_len, al
    pop dx
    pop ax
    pop bp
    ret
input_str ENDP

; get string from buff and store number in ax register.
process_str PROC NEAR
    push cx
    push bx
    push dx
    mov cl, actual_len; initial offset
    dec cx ; cx now stores index offset
    mov dx, 1 ; init multiplier is 1 -> 10 -> 100
    mov bx, 0; result
    xor ax, ax
LOOP1:
    lea si, buff+2
    add si, cx
    mov al, [si]

    cmp al, 30h ; check input block
    jl call_error
    cmp al, 39h
    jg call_error
    
    sub al, 30h ; now al contains the exact number, not the char code.
    push dx ; dx will be overridden by higher byte of mult (dx:ax)
    mul dx ; result in ax
    add bx, ax ; res += number * 10^n
    pop ax ; pop dx -> ax to get proper dx val
    mov dx, 10
    mul dx ; in ax result of 10^n
    mov dx, ax
    dec cx ; decrement counter by 1
    cmp cx, 0
    jge LOOP1 ; if index in cx is >=0 then we haven't reached the end of the loop
    
    mov ax, bx
    pop dx
    pop bx
    pop cx
    ret 

call_error:
    xor ax, ax
    lea ax, message_error
    push ax
    call print_err_fatal
process_str ENDP

; prints error till $. address is on the top of the stack
print_err_fatal PROC NEAR
    push bp
    mov bp, sp ; last pushed param now in [bp+4]. [bp] param is prev bp. [bp+2] is ret addr
    push dx
    push ax
    mov dx, [bp+4]
    mov ah, 09h
    int 21h
    pop ax
    pop dx
    mov bp, [bp]
    mov ah, 4ch ; end the program
    int 21h
print_err_fatal ENDP

; prints nubmer stored on the top of the stack.
print_number PROC NEAR
    push bp
    mov bp, sp
    push ax
    push dx
    push cx
    push bx
    mov cx, 0 ; signs counter
    mov bx, 10
    mov ax, [bp+4] ; value is in ax

LOOP2:    
    xor dx, dx ; make sure dx is clear, because it will store the higher word of [dx:ax] for division
    div bx ; remainder in dx, result in ax
    add dl, 30h ; store ASCII code in dx
    push dx ; push ASCII code on the top of the stack.
    inc cx ; inc signs counter
    cmp ax, 0
    jg LOOP2
    
    mov ah, 2h ; print symbol by symbol
PRINT_LOOP:
    pop dx
    int 21h
    dec cx
    cmp cx, 0
    jg PRINT_LOOP

    pop si
    pop cx
    pop dx
    pop ax
    pop bp
    ret
print_number ENDP

; in stack higher arg would be arg1, lower would be arg2
process_calc PROC NEAR
    push bp
    mov bp, sp
    push bx
    push dx
    xor ax, ax

    mov ah, 09h
    lea dx, message_enter_operation
    int 21h

    mov ah, 01h ; enter one symbol
    int 21h ; symbol is in al
    push ax
    mov ah, 02h
    mov dl, 13
    int 21h
    mov dl, 10
    int 21h
    pop ax
    cmp al, '+'
    je add_label
    cmp al, '-'
    je sub_label
    cmp al, '/'
    je div_label
    cmp al, '*'
    je mul_label
    ; ELSE
    lea ax, message_error_operation
    push ax
    call print_err_fatal

add_label:
    mov ax, [bp+4]
    add ax, [bp+6] ; res in ax
    jmp return
sub_label:
    mov ax, [bp+4]
    sub ax, [bp+6]
    jmp return
div_label:
    xor dx, dx
    mov ax, [bp+4] 
    div [bp+6] ; [dx:ax] / [bp+6] = ax (remainder in dx)
    jmp return
mul_label:
    mov ax, [bp+4]
    mul [bp+6]
    jmp return
return:
    pop dx
    pop bx
    pop bp
    ret
process_calc ENDP

start:
mov    ax, @data
mov    ds, ax 
lea ax, message_enter_first_number ; pre-input invite
push ax
call input_str
call process_str
mov first_number, ax
lea ax, message_enter_second_number ; pre-input invite
push ax
call input_str
call process_str
mov second_number, ax
push second_number
push first_number
call process_calc
push ax
call print_number ; takes value from the stack.
mov    ah,4ch            ; end of program & exit to OS.
int    21h               ; interuption.
END        
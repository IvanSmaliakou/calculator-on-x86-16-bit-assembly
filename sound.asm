.8086
.MODEL SMALL
.STACK 100h ; initialize the stack

; !!! REAL DOSBOX FREQ is ~600 MHz

.DATA  
    SLEEP_RATE dd 0.7
    SLEEP_TIME dw ?

    NOTE_E5  dw 659
    NOTE_G5  dw 784
    NOTE_C5  dw 523
    NOTE_D5  dw 587
    NOTE_F5  dw 698
    NOTE_B3  dw 247
    NOTE_F4  dw 349
    NOTE_G4  dw 392
    NOTE_E4  dw 330
    NOTE_D4  dw 294
    NOTE_A4  dw 440
    NOTE_B4  dw 494

    JINGLE_BELLS_LENGTH_MS DW 125, 125, 250, 125, 125, 250, 125,125,125,125,500, 125,125,125,125, 125,125,125, 63, 63, 125,125,125,125, 250, 250
    JINGLE_BELLS_NOTES DW NOTE_E5, NOTE_E5, NOTE_E5, NOTE_E5,NOTE_E5,NOTE_E5,NOTE_E5,NOTE_G5,NOTE_C5, NOTE_D5,NOTE_E5, NOTE_F5, NOTE_F5, NOTE_F5, NOTE_F5,NOTE_F5, NOTE_E5, NOTE_E5, NOTE_E5, NOTE_E5, NOTE_E5, NOTE_D5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_G5
    JINGLE_BELLS_MELODY_SIZE EQU $-JINGLE_BELLS_NOTES

    WE_WISH_YOU_LENGTH_MS DW 250, 250, 125, 125, 125, 125, 250, 250, 250, 250, 125, 125, 125, 125, 250, 250, 250, 250, 125, 125, 125, 125, 250, 250, 125, 125, 250, 250, 250, 500
    WE_WISH_YOU_NOTES DW NOTE_B3, NOTE_F4, NOTE_F4, NOTE_G4, NOTE_F4, NOTE_E4, NOTE_D4, NOTE_D4, NOTE_D4,NOTE_G4, NOTE_G4, NOTE_A4, NOTE_G4, NOTE_F4,NOTE_E4, NOTE_E4, NOTE_E4,NOTE_A4, NOTE_A4, NOTE_B4, NOTE_A4, NOTE_G4,NOTE_F4, NOTE_D4, NOTE_B3, NOTE_B3, NOTE_D4, NOTE_G4, NOTE_E4, NOTE_F4
    WE_WISH_YOU_MELODY_SIZE EQU $-WE_WISH_YOU_NOTES
    
.CODE 
    mov ax, @DATA
    mov ds, ax
    jmp INPUT_LOOP

INPUT_LOOP:
    mov ah, 08h
    int 21h
    cmp al, '1'
    je JINGLE_BELLS_LABEL
    cmp al, '2'
    je WE_WISH_YOU_MC_LABEL
    
    mov ah,4ch            ; end of program & exit to OS.
    int 21h               ; interuption.

JINGLE_BELLS_LABEL:
    push JINGLE_BELLS_MELODY_SIZE
    push offset JINGLE_BELLS_LENGTH_MS
    push offset JINGLE_BELLS_NOTES
    call play_song
    add sp, 6 ; remove 3 args from the stack
    jmp INPUT_LOOP

WE_WISH_YOU_MC_LABEL:
    push WE_WISH_YOU_MELODY_SIZE
    push offset WE_WISH_YOU_LENGTH_MS
    push offset WE_WISH_YOU_NOTES
    call play_song
    add sp, 6 ; remove 3 args from the stack
    jmp INPUT_LOOP

; STDCALL
; 1-st arg is a ptr to the beginning of the array of the song freq
; 2-nd arg is a ptr to the beginning of the array of the song notes length
; 3-rd arg is the size of the song in notes
; plays a song
PROC play_song
    push bp
    mov bp, sp
    push ax
    push bx
    push cx
    push dx
    push si
    push di

    mov bx, 0
    jmp MELODY_LOOP

MELODY_LOOP:
    mov si, [bp+6] ; address of the note length array start.
    push [si + bx] ; note length + array offset. CAN BE ONLY BX!
    mov si, [bp+4] ; address of the note freq array start.
    mov di, [si + bx] ; note freq in Hz + array offset. CAN BE ONLY BX!
    push [di]
    call sound

    fld dword ptr[SLEEP_RATE] ; load SLEEP_RATE into logical soproc. stack
    mov di, [bp+6]
    fimul word ptr[di + bx] ; SLEEP_RATE * [JINGLE_BELLS_LENGTH_MS+offset] = ST(0)
    fistp SLEEP_TIME ; get ST(0)

    push word ptr[SLEEP_TIME]
    call sleep

    add sp, 6 ; remove length, and 2 args from sound() proc call from the stack 
    add bx, 2 ; increment by 2, because arrays consist of 2-bytes values.

    cmp bx, [bp+8] ; check if arrays offset is lower than the size of arrays in bytes
    jl MELODY_LOOP

    pop di
    pop si
    pop dx
    pop cx
    pop bx
    pop ax
    pop bp
    ret
play_song ENDP

; on the top of the stack is wait time in milliseconds
PROC sleep NEAR
    push bp
    mov bp, sp
    push ax
    push dx
    push cx

    mov ax, [bp+4] ; first arg from stack
    mov dx, 1000
    mul dx ; result in [dx:ax]
    mov cx, dx ; 
    mov dx, ax ; sleep time in microseconds is in [cx:dx]
    mov ah, 86h ; sleep func and 15h port interuption
    int 15h

    pop cx
    pop dx
    pop ax
    pop bp
    ret
sleep ENDP

; STDCALL!
; frequency in Hz is the first arg on the stack
; length in miliseconds is the 2-nd arg on the stack
PROC sound NEAR
    push bp
    mov bp, sp
    push bx
    push ax
    xor bx, bx
    xor ax, ax

    mov al, 182
    out 43h, al ; prepare speaker for the note.
    mov bx, [bp+4]
    mov dx, 0012h ; we need to send to port 42h 1193180/freq. So we divide 1234DCh/freq.
    mov ax, 34DCh
    div bx  
    out 42h, al; lower byte to 42h port
    mov al, ah ; register mush be either ax or al
    out 42h, al ; higher byte to 42h port
    in al, 61h ; get value from port 61h
    or al, 00000011b ; set 2 lower bits
    out 61h, al ; set new value
    mov ax, [bp+6] ; load wait time in microseconds in [cx:dx]
    mov dx, 1000
    mul dx ; wait time in mictos in [dx:ax]
    mov cx, dx
    mov dx, ax
    mov ah, 86h ; 86h is BIOS wait function
    int 15h ; wait interuption
    in al, 61h ; get speaker value
    and al, 11111100b ; reset the 2 lower bits
    out 61h, al ; send the value

    pop ax
    pop bx
    pop bp
    ret
sound ENDP
END

